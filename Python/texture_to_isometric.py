#!/usr/bin/env python3

from PIL import Image
import os

print("Resize values (pixel):")
w = input("width : ")

if w <= 0:
    w = 32

# Execute in your directory
for filename in os.listdir("."):
    if filename.endswith(".jpg") or filename.endswith(".png"):
        with Image.open(filename) as img:
            # Rotate 45d to isometric position
            img = im.rotate(45, expand=True)
            
            width, height = im.size
            
            # Isometric proportional 1:1/2
            new_height = int(width * 0.5)
            
            new_im = im.resize((w, int(width * 0.5)))
            # Salvar a imagem como um arquivo PNG
            new_filename = os.path.splitext(filename)[0] + "_isometric.png"
            new_im.save(new_filename, "PNG")

