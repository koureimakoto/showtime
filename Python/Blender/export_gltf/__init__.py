import os
from bpy.ops import export_scene

def empty(string):
    return string == ''

class NFT_EXPORTER:
    __name  : str = ''
    __path  : str = ''
    __suffix: str = ''
    __fmt   : str = ''
    
    def set_name(self, file_name):
        if not empty(file_name):
            self.__name =  file_name
    
    def file_name(self):
        return self.__name
    
    def set_path_from_home(self, file_path):
        if not empty(file_path):
            self.__path = file_path
    
    def file_path(self):
        return self.__path
    
    def output_format(self, type):
        type = type.lower()
        if not empty(type): 
            if 'gltf' == type:
                self.__fmt = 'GLTF_EMBEDDED'
            elif 'glb' == type:
                self.__fmt = 'GLB'
            else:
                print('Invalid GLTF or GLB format')
                return False 
            self.__suffix = type
            return True
        
        print('Empty Output Format')
        return False
            
    def __str__(self):
        user_path = os.path.join(os.environ['HOME'], "{}/{}.{}".format(self.__path, self.__name, self.__suffix))
        return user_path
    
    def export(self):
        export_scene.gltf(
            filepath=self.__str__(),
            export_format=self.__fmt
        )
        
    
exporter = NFT_EXPORTER()
exporter.set_name('file_n')
exporter.set_path_from_home('henri_clones')
exporter.output_format('gltf')
print(exporter.file_name())
print(exporter.file_path())
print(exporter)

exporter.export()