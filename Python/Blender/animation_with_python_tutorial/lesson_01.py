import bpy
from math import radians

def reverse():
    buffer: float = bpy.context.scene.frame_current
    bpy.ops.screen.animation_cancel()
    
    bpy.context.scene.frame_set(buffer)
    bpy.ops.screen.animation_play(reverse=True)
    return None

# https://docs.blender.org/api/current/bpy.types.html
cubes : bpy.types.Object = bpy.data.collections["Collection"].objects
cube_string_size: int    = len(cubes)

rotate      : float = 0
rotate_const: float = radians(360.0 / cube_string_size) 

start_frame      : float = 1.0
cur_frame        : float = 0
end_frame        : float = 240
frames_per_second: float = 24.0

MAX_ROTATION     : float = radians(360)

# Set initicial keyframe
for cur_cube in cubes:
    
    # Set start object keyframe for rotation_euler
    cur_cube.keyframe_insert(
        data_path="rotation_euler",
        frame=cur_frame
    )
    cur_frame += frames_per_second
    
    # Set end object keyframe
    cur_cube.rotation_euler[0] = MAX_ROTATION
    cur_cube.keyframe_insert(
        data_path="rotation_euler",
        frame=end_frame
    )


bpy.app.timers.register(
    reverse,
    first_interval=(end_frame/frames_per_second - 1.0)
)

bpy.ops.screen.animation_play()


"""
for cur_cube in cubes:
    # .location[0] = x: float 
    # .location[1] = y: float
    # .location[2] = z: float
    # OR .location = [x, y, z]
    cur_cube.location[2] = 2.0
    
    # The same logic of the location for euler calc.
    # if I use quarternion my array  goes to index 3
    # Each of the  index need to  convert to radians 
    # using (x.PI / 180) or using math library with
    # function radians().
    cur_cube.rotation_euler[0] = rotate
    rotate -= r
    otate_const
    cur_cube.keyframe_insert(data_path="rotation", frame)
"""
