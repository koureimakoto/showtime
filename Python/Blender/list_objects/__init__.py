import bpy
import re

# Only find the collection using suffix .nft 
def list_nft_collections():
    buffer = []
    for collection in bpy.data.collections:
        if re.findall('.*.nft', collection.name):
            buffer.append(collection)
    
    return buffer

def print_nft_assets(collections):
    for collection in collections:
        print(collection.name)
        for asset in collection.all_objects:
            print(asset.name)
        
        
collections_nft = list_nft_collections()

print("\n\n\n\ List")
print_nft_assets(collections_nft)

