import bpy
import re
import random

# Only find the collection using suffix .nft 
def list_nft_collections():
    buffer = []
    for collection in bpy.data.collections:
        if re.findall('.*.nft', collection.name):
            buffer.append(collection)
    
    return buffer

def print_nft_assets(collections):
    for collection in collections:
        print(collection.name)
        for asset in collection.all_objects:
            print(asset.name)
        

collections_nft = list_nft_collections()

print("\n\n\nLIST:")
print_nft_assets(collections_nft)

def random_item(collection):
    num = random.randint(0, len(collection.all_objects) - 1)
    return collection.all_objects.keys()[num]
    
def object_viewport_visibility(collection, name, status):
    collection.all_objects[name].hide_viewport = status
    collection.all_objects[name].hide_render   = status
    
def hide(collection, name):
    object_viewport_visibility(collection, name, True)
    
def show(collection, name):
    object_viewport_visibility(collection, name, False)
    
print("\nRandom HEAD")
print(random_item(collections_nft[0]))

print("\nRandom BODY")
print(random_item(collections_nft[1]))

print("\nRandom PONTEIRO")
print(random_item(collections_nft[2]))

value = random_item(collections_nft[0])
print(value)
#show(collections_nft[0], value)