from bpy.types import Operator, Panel, WindowManager, DATA_PT_modifiers
from bpy.props import BoolProperty
from bpy       import context

C = context

class MyOperator(Operator):
    bl_space_type  = 'PROPERTIES'
    bl_region_type = 'NAVIGATION_BAR'
    bl_idname      = "my.op"
    bl_label       = ""
    bl_icon        = "EYEDROPPER"
    
    
    def execute(self, context):
        # Define what is the current context
        """
        
        In this moment blender set da space_type data with a context
        but it only work if there is a context with the same name.
        
        I can't add a new context for PROPERTIES/NAVIGATION_BAR
        unless I change de enum in C and recompile. I maybe wrong,
        but ...

        Or create a new state using a global var for each context
        inside NAVIGATION_BAR.
        
        """
        C.space_data.context = 'MODIFIER'
        C.window_manager.nft = True
        C.window_manager.nft_event = True

        return {'FINISHED'}
    
    def invoke(self, context, event):
        self.x = event.mouse_x
        self.y = event.mouse_y
        return self.execute(context)

# Create my owner Icon inside Properties Navigation Bar
class MY_PT_context(Panel):
    bl_space_type  = 'PROPERTIES'
    bl_region_type = 'NAVIGATION_BAR'
    bl_options     = {'HIDE_HEADER'}
    bl_context     = 'my_context'
    bl_label       = ""
    
    @classmethod
    def poll(cls, context):
        return context.scene
    
    def draw(self, context):
        layout = self.layout
        layout.scale_x = 1.2
        layout.scale_y = 1.4
        layout.operator(MyOperator.bl_idname, text="", icon=MyOperator.bl_icon)

class MY_PT_panel(Panel):
    bl_space_type  = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_label       = ""
    bl_context     = 'modifier'

    @classmethod
    def poll(cls, context):d
        if C.window_manager.nft == True:
            print('chamou')
            C.window_manager.nft_event = True
            return True
        return False

    def draw(self, _context):
#        if C.window_manager.nft == True and C.window_manager.nft_mod == False:
        self.layout.label(text="Hello World")
        

classes = (
    MyOperator,
    MY_PT_context,
    MY_PT_panel,
)
        
def register(cl):
    bpy.utils.register_class(cl)

def unregister(cl):
    bpy.utils.unregister_class(cl)
    
if __name__ == "__main__":
    from bpy.utils import register_class
    
    WindowManager.nft       = BoolProperty(default=False)
    WindowManager.nft_mod   = BoolProperty(default=False)
    WindowManager.nft_event = BoolProperty(default=False)
    
    for cl in classes:
        register_class(cl)
        #unregister(cl)
        
#bpy.utils.register_module(__name__)