# SPDX-License-Identifier: GPL-2.0-or-later
from bpy.types import Panel, UILayout
from bpy import context

C = context

class ModifierButtonsPanel:
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "modifier"
    bl_options = {'HIDE_HEADER'}
   
def my_draw(self, context):
    layout = self.layout

class DATA_PT_modifiers(ModifierButtonsPanel, Panel):
    bl_label = "Modifiers"
    redraw   = False
    
    def __init__(self):
        self.old = UILayout(self.layout.type_recast())
    
    @classmethod
    def poll(cls, context):
        if C.window_manager.nft_mod == True and C.window_manager.nft == False:
            ob = context.object
            return ob and ob.type != 'GPENCIL'
        return False
    
    # Try out draw my panel individualy
    def draw(self, _context):
        layout = self.layout
        if C.window_manager.nft_mod == True and C.window_manager.nft == False:
            layout.emboss = 'NORMAL'
            layout.operator_menu_enum("object.modifier_add", "type")
            layout.operator_menu_enum("object.modifier_add", "type")
            layout.template_modifiers()
        else:
            layout.emboss = 'NONE'
        
#    

#    def draw(self, _context):
#        layout = self.layout
#        if C.window_manager.nft == True:
#            self.draw = self.first
#        else:
#            self.draw = self.second

#        self.redraw = True
#        C.window_manager.nft = False
#        C.area.tag_redraw()
#            
#    def draw(self, _context):
#        layout = self.layout
#        layout.operator_menu_enum("object.modifier_add", "type")
#        layout.template_modifiers()
#        C.window_manager.nft = False


class DATA_PT_gpencil_modifiers(ModifierButtonsPanel, Panel):
    bl_label = "Modifiers"

    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob and ob.type == 'GPENCIL'

    def draw(self, _context):
        layout = self.layout
        layout.operator_menu_enum("object.gpencil_modifier_add", "type")
        layout.template_grease_pencil_modifiers()

classes = (
    DATA_PT_modifiers,
    DATA_PT_gpencil_modifiers,
)

if __name__ == "__main__":  # only for live edit.
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)
